package main

import (
    "fmt"
    "text/template"
    "os"
    // "reflect"
    "strings"

    "github.com/xuri/excelize/v2"
)

type Ec2Tag struct {
    TagKey   string
    TagValue string
}

type Ec2Properties struct {
    Name                string
    Ami                 string
    InstanceType        string
    KeyName             string
    Monitoring          bool
    VpcSecurityGroupIds []string
    SubnetId            string
    Tags                []Ec2Tag
}

func CompileRds() {
    fmt.Println("TODO: Compile RDS")
}


func CompileEc2(file *excelize.File) {
    sheetList := file.GetSheetList()
    rows, err := file.GetRows(sheetList[0])
    if err != nil {
        panic(err)
    }

    data := Ec2Properties{}

    for i := 3; i < len(rows); i++ {
        key := rows[i][0]
        value := rows[i][1]

        switch key {
        case "Name":
            data.Name = value
        case "Ami":
            data.Ami = value
        case "InstanceType":
            data.InstanceType = value
        case "KeyName": 
            data.KeyName = value
        case "Monitoring":
            if value == "0" {
                data.Monitoring = false
            } else { data.Monitoring = true }
        case "VpcSecurityGroupIds":
            ids := strings.Split(value, ",")
            for id := range ids {
                data.VpcSecurityGroupIds = append(data.VpcSecurityGroupIds, ids[id])
            }
        case "SubnetId":
            data.SubnetId = value
        case "Tags":
            tags := strings.Split(value, ",")
            for t := range tags {
                tag := strings.Split(tags[t], ":")
                newTag := Ec2Tag{TagKey: tag[0], TagValue: tag[1]}
                data.Tags = append(data.Tags, newTag)
            }
        }
    }

    filename := fmt.Sprintf("./%s.tf", data.Name)
    newFile, err := os.Create(filename)
    if err != nil {
        panic(err)
    }

    t, err := template.ParseFiles("ec2.tmpl")
    if err != nil {
        panic(err)
    }

    err = t.Execute(newFile, data)
}

func main() {
    ef, err := excelize.OpenFile("sampleasr.xlsx")
    if err != nil {
        panic(err)
    }

    sheetList := ef.GetSheetList()
    rows, err := ef.GetRows(sheetList[0])
    if err != nil {
        panic(err)
    }

    resourceType := rows[2][1]

    switch resourceType {
    case "ec2":
        CompileEc2(ef)
    case "rds":
        CompileRds()
    }
}
